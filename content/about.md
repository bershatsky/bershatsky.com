---
date: 2018-12-28T21:46:00
title: "About"
---
There's so much information on the web nowadays. Some of the information is quite helpful! However, other bits of information are confusing, factually inaccurate, out of date, wrong (partially or completely), biased, and even misleading too. Quite often, the information is just expressed in an overly verbose way. Online forums have similar problems, and are often marred by rudeness. 

These issues were the catalyst that caused me to start my own blog. This is a place where I can share what I've learned, and get input from knowledgeable others. My hope is that you'll find these articles helpful.

If you have any questions or comments, please don't hesitate to reach out!