---
title: "Netlify.com is Amazing!"
date: 2019-02-18T14:26:00-08:00
tags: ["Hugo"]
---

Maybe you already knew this, but Netlify is better than AWS!

I'm new to this whole thing, and have learned the following:

1) GitHub has a 1GB limit for hosting and thus, probably won't work for me.
2) AWS is terribly complicated, but even after you get it configured, pretty URLs are a pain! There's something called Lambda which I fought with for hours today and couldn't get working.
3) Netlify gives 100GB of storage for free! It's easy to upload - and works!

Seriously, I'm not sure why anyone would use anything else!!!